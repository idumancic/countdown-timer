const app = {};

// TODO: add before and after view of possible number, like an Apple timer
// TODO: implement audio clock tick on number change

(function (app, $) {

    'use strict';

    let timer, progressBar,
        $countdownTimer, $countdownShow, $hours,
        $minutes, $seconds, $btnStart,
        $btnPause, $btnResume, $btnCancel;

    app.onInit = function () {
        setControls();
        setProgressBar();
        setControlListeners();
        setInitialControlStates();
        setBackgroundTheme();
    }

    app.onStartClick = function (e) {
        const hours = parseInt($hours.val());
        const minutes = parseInt($minutes.val());
        const seconds = parseInt($seconds.val());

        if (hours === 0 && minutes === 0 && seconds === 0) {
            alert("You have to set countdown time for timer to begin.")
            return false;
        }

        $countdownTimer.hide();
        $countdownShow.show();

        $btnStart.hide();
        $btnPause.show();
        $btnCancel.prop('disabled', false);

        progressBar.set(1.0);
        const finishTime = new Date((Date.now() + (Timer.toSeconds({ hours, seconds, minutes }) * 1000)))
        const finishTimeHtmlString = `<span class="cd-finish-time">${appendZero(finishTime.getHours())}:${appendZero(finishTime.getMinutes())}</span>`;

        timer = new Timer(hours, minutes, seconds)
            .onFinish(() => app.onStopClick({ lastTick: true }))
            .onTick(({ hours, minutes, seconds }) => {
                let value = Timer.toSeconds({ hours, minutes, seconds })
                progressBar.animate(map(value, 0, timer.duration, 0, 1), { duration: 1000 });

                const timeString = `${appendZero(hours)}:${appendZero(minutes)}:${appendZero(seconds)}`;
                progressBar.setText(timeString + finishTimeHtmlString);
            })

        timer.start();
    }

    app.onResumeClick = function () {
        timer.resume();
        $btnResume.hide();
        $btnPause.show();
    }

    app.onPauseClick = function () {
        timer.pause()
        $btnPause.hide();
        $btnResume.show();
    }

    app.onStopClick = function ({ lastTick = false }) {
        timer.stop();

        setTimeout(() => {
            $countdownShow.hide();
            $countdownTimer.show();
            $btnStart.show();
            $btnPause.hide();
            $btnResume.hide();
            $btnCancel.prop('disabled', true);
        }, lastTick === true ? 1000 : 0)
    }

    function setControls() {
        $countdownTimer = $('.cd-timer');
        $countdownShow = $('.cd-show');
        $hours = $('#hours');
        $minutes = $('#minutes');
        $seconds = $('#seconds');
        $btnStart = $('#btnStart');
        $btnPause = $('#btnPause');
        $btnResume = $('#btnResume');
        $btnCancel = $('#btnCancel');
    }

    function setControlListeners() {
        $btnStart.click(app.onStartClick)
        $btnPause.click(app.onPauseClick)
        $btnResume.click(app.onResumeClick)
        $btnCancel.click(app.onStopClick)
    }

    function setProgressBar() {
        progressBar = new ProgressBar.Circle('#progress-bar', {
            color: '#000',
            strokeWidth: 2,
            trailWidth: 1,
            easing: 'easeInOut',
            text: {
                autoStyleContainer: false,
            },
            from: { color: '#f39842', width: 1 },
            to: { color: '#f39842', width: 2 },
            step: function (state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);
            }
        });
    }

    function setInitialControlStates() {
        $countdownShow.hide();
        $btnCancel.prop('disabled', true);
        $btnPause.hide();
        $btnResume.hide();

        fillTimeOptions($hours, 0, 23)
        fillTimeOptions($minutes, 0, 59)
        fillTimeOptions($seconds, 0, 59)
    }

    function setBackgroundTheme() {
        const matchMediaDark = window.matchMedia('(prefers-color-scheme: dark)')
        const toggleDarkTheme = ({ matches }) => matches ? $('body').addClass("dark") : $('body').removeClass("dark");
        matchMediaDark.addListener(toggleDarkTheme);
        toggleDarkTheme(matchMediaDark);
    }

    function map(value, in_min, in_max, out_min, out_max) {
        return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    function appendZero(time) {
        return time < 10 ? '0' + time : time;
    }

    function fillTimeOptions($select, start, end) {
        for (let i = start; i <= end; i++) {
            $select.append('<option value="' + i + '">' + appendZero(i) + '</option>');
        }
    }

})(app, jQuery);

$(app.onInit)




