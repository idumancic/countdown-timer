// https://stackoverflow.com/questions/20618355/the-simplest-possible-javascript-countdown-timer
class Timer {
    static parse(seconds) {
        return {
            hours: (seconds / 3600) | 0,
            minutes: ((seconds % 3600) / 60) | 0,
            seconds: (seconds % 60) | 0
        }
    }

    static toSeconds({ hours, minutes, seconds }) {
        return (hours * 3600) + (minutes * 60) + seconds;
    }

    constructor(hours, minutes, seconds) {
        this._hours = hours;
        this._minutes = minutes;
        this._seconds = seconds;
        this._tickListeners = [];
        this._finishListeners = [];
        this._countDownInterval = null;
        this._running = false;
        this._paused = false;
        this._lastTick = null;
    }

    get duration() {
        return Timer.toSeconds({ hours: this._hours, minutes: this._minutes, seconds: this._seconds })
    }

    get expired() {
        return this._running === false
    }

    start() {
        if (this._running === true || this._paused === true) {
            console.warn("Timer is already running...");
            return;
        }

        this._running = true;
        const start = Date.now();
        const duration = this._lastTick ? Timer.toSeconds(this._lastTick) : this.duration;
        const countdown = () => {
            let diff = duration - (((Date.now() - start) / 1000) | 0);
            let tick = Timer.parse(diff);

            this._lastTick = tick;
            this._tickListeners.forEach(x => x.call(this, tick), this);

            if (diff <= 0) {
                clearInterval(this._countDownInterval);
                this._lastTick = null;
                this._running = false;
                this._finishListeners.forEach(x => x.call(this), this);
            }
        }

        countdown()
        this._countDownInterval = setInterval(countdown, 1000);
    }

    stop() {
        this._running = false;
        clearInterval(this._countDownInterval);
    }

    resume() {
        this._paused = false;
        this.start()
    }

    pause() {
        this._paused = true;
        this._running = false;
        clearInterval(this._countDownInterval);
    }

    onTick(listener) {
        if (typeof listener !== typeof Function) {
            console.warn("Listener param has to be type of Function");
            return;
        }

        this._tickListeners.push(listener);
        return this;
    }

    onFinish(listener) {
        if (typeof listener !== typeof Function) {
            console.warn("Listener param has to be type of Function");
            return;
        }

        this._finishListeners.push(listener);
        return this;
    }
}